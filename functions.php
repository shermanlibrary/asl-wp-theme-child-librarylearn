<?php

require_once('library/academy-video-type.php'); // you can disable this if you like
require_once('library/custom-fields.php');

/* ==================
 * Helper Functions
 * ================== */

/* ==================
 * Allow Captions to be Uploadable */
function add_upload_mime_types( $mimes ) {
    //if ( function_exists( 'current_user_can' ) ) {
    //    $unfiltered = $user ? user_can( $user, 'unfiltered_html' ) : current_user_can( 'unfiltered_html' );
    //}

    //if ( !empty( $unfiltered) ) {
        $mimes['srt'] = 'text/plain';
        $mimes['vtt'] = 'text/vtt';
    //}

    return $mimes;

} add_filter( 'upload_mimes', 'add_upload_mime_types' );


/* ==================
 * $DESCENDANT CATEGORY */
/**
 * Tests if any of a post's assigned categories are descendants of target categories
 *
 * @param int|array $cats The target categories. Integer ID or array of integer IDs
 * @param int|object $_post The post. Omit to test the current post in the Loop or main query
 * @return bool True if at least 1 of the post's categories is a descendant of any of the target categories
 * @see get_term_by() You can get a category by name or slug, then pass ID to this function
 * @uses get_term_children() Passes $cats
 * @uses in_category() Passes $_post (can be empty)
 * @version 2.7
 * @link http://codex.wordpress.org/Function_Reference/in_category#Testing_if_a_post_is_in_a_descendant_category
 */
if ( ! function_exists( 'post_is_in_descendant_category' ) ) {
    function post_is_in_descendant_category( $cats, $_post = null ) {
        foreach ( (array) $cats as $cat ) {
            // get_term_children() accepts integer ID only
            $descendants = get_term_children( (int) $cat, 'category' );
            if ( $descendants && in_category( $descendants, $_post ) )
                return true;
        }
        return false;
    }
}

/* ==================
 * $POST-TYPES in RESULTS
 */ // Let's not discriminate ...
function namespace_add_custom_types( $query ) {
  if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
    $query->set( 'post_type', array(
     'post', 'academy_video', 'nav_menu_item'
        ));
      return $query;
    }
}
add_filter( 'pre_get_posts', 'namespace_add_custom_types' );

/* ==================
 * Search
 * ================== */
// Search Form
function search_librarylearn_for_videos() {
    $form = '<form class="has-background no-margin form" role="search" method="get" id="searchform" action="' . home_url( '/' ) . '">
        <ul class="no-margin">
            <li class="form__field no-margin">
                <label class="hide-accessible label" for="searchsubmit">Browse for Videos</label>
                <svg class="svg svg--search" style="margin-right: 1em;" viewBox="0 0 32 32"><use xlink:href="#icon-search"></use></svg>
                <input class="input form__input form__input--full-width search__search-field search__search-field--transparent input--large input--epsilon" type="search" value="' . get_search_query() . '" name="s" id="s" placeholder="'.esc_attr__('Search for a video ...','bonestheme').'" />
            </li>
        </ul>
    </form>';
    return $form;
} // don't remove this bracket!

/*********************
RELATED POSTS FUNCTION
*********************/

// Related Posts Function (call using bones_related_posts(); )
function library_related_videos() {
    global $post;
    $tags = wp_get_post_tags($post->ID);

    if($tags) {

        $tag_ids = array();

        foreach($tags as $tag) { $tag_ids[] = $tag->term_id; }

        $args = array(
            'tag__in' => $tag_ids,
            'posts_per_page' => 3,
            'post__not_in' => array($post->ID),
            'post_type' => 'academy_video'
        );

        $related_posts = new WP_Query($args);

        while( $related_posts->have_posts() ) {

            $related_posts->the_post();

            ?>

                <article class="card--alt media">
                    <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">

                        <?php echo the_post_thumbnail( 'media-small' ); ?>
                    </a>

                        <header>
                            <h3 class="epsilon"><?php the_title(); ?></h3
                        </header>
                    </a>
                        <p class="zeta no-margin"><?php echo get_the_excerpt();?></p>

                </article>

    <?php } }
    wp_reset_query();
} /* end bones related posts function */

wp_enqueue_style( 'wp-mediaelement' );

/**
 * Auto-subscribe or unsubscribe an Edit Flow user group when a post changes status
 *
 * @see http://editflow.org/extend/auto-subscribe-user-groups-for-notifications/
 *
 * @param string $new_status New post status
 * @param string $old_status Old post status (empty if the post was just created)
 * @param object $post The post being updated
 * @return bool $send_notif Return true to send the email notification, return false to not
 */

function asl_wp_librarylearn_auto_subscribe_usergroup( $new_status, $old_status, $post ) {

    global $edit_flow;

    // When the post is first created, you might want to automatically set
    // all of the user's user groups as following the post
    if ( 'draft' == $new_status ) {

        // Get all of the user groups for this post_author
        $usergroup_ids_to_follow = $edit_flow->user_groups->get_usergroups_for_user( $post->post_author );
        $usergroup_ids_to_follow = array_map( 'intval', $usergroup_ids_to_follow );
        $edit_flow->notifications->follow_post_usergroups( $post->ID, $usergroup_ids_to_follow, true );
    }

    // You could also follow a specific user group based on post_status
    if ( 'pending' == $new_status ) {
        // You'll need to get term IDs for your user groups and place them as
        // comma-separated values
        $usergroup_ids_to_follow = array(
                // 18,
                148,
            );
        $edit_flow->notifications->follow_post_usergroups( $post->ID, $usergroup_ids_to_follow, true );
    }

    // Return true to send the email notification
    return $new_status;
}
add_filter( 'ef_notification_status_change', 'asl_wp_librarylearn_auto_subscribe_usergroup', 10, 3 );

add_action( 'pre_get_posts', 'asl_wp_librarylearn_alphabetize_archives' );
function asl_wp_librarylearn_alphabetize_archives( $query ) {
    if ( is_archive() || is_post_type_archive() ) :
        $query->set( 'order', 'ASC' );
        $query->set( 'orderby', 'title' );
    endif;
}

function shortcode_to_embed_tutorials_in_modules( $atts ) {


  extract( shortcode_atts( array(
    'name' => '',
    'order' => ''
  ), $atts ));

  return '<section class="clearfix module" data-ng-controller="ModuleController as mc" data-name="' . $atts[ 'name' ] . '" data-order="' . $atts[ 'order' ] . '">
            <div class="col-sm--twocol col-lg--onecol" aria-hidden="true"></div>
            <div class="col-sm--tencol col-lg--elevencol">
              <div class="col-lg--fourcol media" style="padding-right: .5em;">
                <a class="link link--undecorated" data-ng-click="mc.watch( $event )" href="{{ mc.tutorial.url }}">
                  <img data-ng-src="{{ mc.tutorial.thumbnail }}">
                </a>
              </div>
              <div class="col-lg--eightcol">
                <a class="link link--undecorated" href="{{ mc.tutorial.url }}" data-ng-click="mc.watch( $event )">
                  <h2 class="delta no-margin">{{ mc.tutorial.title }}</h2>
                </a>
                <p class="zeta no-margin">{{ mc.tutorial.excerpt }}</p
              </div>
            </div>

            <section class="modal semantic-content" data-ng-class="{ \'is-active\' : mc.modalToggle }" role="dialog" ng-attr-aria-hidden="{{ modalToggle ? \'false\' : \'true\' }}">
              <div class="modal-inner">
                <div class="clearfix modal-content no-padding">

                  <video height="100%" width="100%" controls itemprop="video" ng-src="{{ mc.videoUrl }}">
          				  <track kind="subtitles" src="{{ mc.tutorial.captions }}" srclang="en" label="English">
            				<p class="alert alert--danger">Sorry. Your browser doesn\'t support HTML5 video.</p>
            			</video>

                </div>
                <footer>
                  <p class="align-center no-margin zeta">{{ mc.tutorial.title }}</p>
                </footer>
              </div>
              <button class="modal-close" data-ng-click="mc.watch( $event )">Close</button>
            </section>
          </section>';
}
add_shortcode( 'tutorial', 'shortcode_to_embed_tutorials_in_modules' );

function create_module_api_endpoint( WP_REST_Request $request ) {

$count = $request[ 'count' ];
$search = $request[ 'search' ];

 $args = array(
   'post_type' => 'module',
   'posts_per_page' => ( $count ? $count : 5 ),
   's' => ( $search ? $search : '' )
 );

 $posts = get_posts( $args );
 $return = array();

 foreach( $posts as $post ){

   $return[] = array(

     'title' => $post->post_title,
     'excerpt' => $post->post_excerpt,
     'url' => get_permalink( $post->ID )

   );

 }

 if ( empty ( $posts ) ) {
   return null;
 }

 $response = new WP_REST_Response( $return );
 return $response;

}

function create_tutorial_api_endpoint( WP_REST_Request $request ) {

$name = $request['slug'];

 $args = array(
   'post_type' => 'academy_video',
   'name' => $name
 );

 $posts = get_posts( $args );
 $return = array();

 foreach( $posts as $post ){

   $return[] = array(

     'title' => $post->post_title,
     'captions' => wp_get_attachment_url( get_post_meta( $post->ID, 'captions', true ) ),
     'excerpt' => $post->post_excerpt,
     'thumbnail' => wp_get_attachment_url( get_post_thumbnail_id( $post->ID) ),
     'url' => get_permalink( $post->ID ),
     'video' => get_post_meta( $post->ID, 'academy_video_file', true )

   );

 }

 if ( empty ( $posts ) ) {
   return null;
 }

 $response = new WP_REST_Response( $return );
 return $response;

}

function create_tutorial_and_module_api_route() {
  register_rest_route( 'tutorials/v2', '/tutorial', array(
    'methods' => 'GET',
    'callback' => 'create_tutorial_api_endpoint'
  ));

  register_rest_route( 'modules/v2', '/module', array(
    'methods' => 'GET',
    'callback' => 'create_module_api_endpoint'
  ));

}

add_action( 'rest_api_init', 'create_tutorial_and_module_api_route' );
