<?php get_header(); ?>


<div id="content" itemscope itemtype="http://schema.org/VideoObject">

	<section class="background-base clearfix has-background media">

		<div class="clearfix wrap" style="padding-top: 1em; padding-bottom: 1em;">

		<?php
		if (have_posts()) : while (have_posts()) : the_post();
		$video = get_post_meta( get_the_ID(), 'academy_video_file', true);
		?>

			<video class="shadow" poster="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" height="100%" width="100%" autoplay="false" controls itemprop="video">

				<source type='video/webm; codecs="vp8, vorbis"' src="//www.nova.edu/library/video/<?php echo $video; ?>.webm" />
				<source type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' src="//www.nova.edu/library/video/<?php echo $video ?>.mp4" />
				<track kind="subtitles" src="<?php echo wp_get_attachment_url( get_post_meta( get_the_ID(), 'captions', true ) ); ?>" srclang="en" label="English">

				<p class="alert alert--danger">Sorry. Your browser doesn't support HTML5 video.</p>

			</video>

		</div>

	</section>

	<section class="has-cards hero--small clearfix">

		<div class="wrap clearfix">

			<p class="align-center zeta">
				All videos © <a href="http://www.nova.edu">Nova Southeastern University</a> 2016
			</p>

			<div class="col-md--eightcol">

				<article class="card--alt" role="article">

					<header>

						<h1 class="beta" itemprop="name"><?php the_title(); ?></h1>

					</header>

					<section>

						<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemprop="transcript">

							<section class="post-content">

								<p><?php echo get_the_excerpt(); ?></p>

								<?php the_content(); ?>

							</section> <!-- end article section -->

						</article> <!-- end article -->

					</section>

				</article>

			</div><!--/.eightcol-->

			<div class="col-md--fourcol">
				<h2 class="gamma">Related Videos</h2>
				<?php library_related_videos(); ?>
			</div>

		</div>

	</section><!--/.has-cards-->

</div> <!-- end #content -->

	<?php endwhile; ?>
	<?php endif; ?>

<?php get_footer(); ?>
