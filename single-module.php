<?php get_header(); ?>
<div id="content" ng-app="moduleApp">
	<div class="clearfix">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


			<div id="main" class="clearfix" role="main">
				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

					<header class="hero--small card card--shadow" style="border: none;">
            <div class="clearfix wrap">
              <div class="col-md--tencol col-lg--eightcol col--centered">
    						<h1 class="beta" itemprop="headline"><?php the_title(); ?></h1>
                <p><?php echo get_the_excerpt(); ?></p>
              </div>
            </div>
					</header> <!-- end article header -->

					<section class="has-cards hero--small" itemprop="articleBody">
            <div class="clearfix wrap">
              <div class="col-md--tencol col-lg--eightcol col--centered clearfix">
  						<?php the_content(); ?>
              </div>
            </div>
					</section> <!-- end article section -->

				</article> <!-- end article -->

			<?php endwhile; ?>

			<?php else : ?>

				<article id="post-not-found" class="hentry clearfix">
		    		<header class="article-header">
		    			<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
		    		</header>
		    		<section class="post-content">
		    			<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
		    		</section>
		    		<footer class="article-footer">
		    		    <p><?php _e("This is the error message in the single.php template.", "bonestheme"); ?></p>
		    		</footer>
				</article>

			<?php endif; ?>

		</div> <!-- end #main -->

		<?php // get_sidebar(); // sidebar 1 ?>

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>

<script src="//sherman.library.nova.edu/cdn/scripts/min/ng-route-sanitize-cookies-1.5.7.min.js"></script>
<script type="text/javascript">

var app = angular.module('moduleApp', [ 'ngCookies', 'ngSanitize' ]);

(function() {
  'use strict';

  /**
   * Data Services
   *
   * Use data services to communicate with internal or external APIs
   * and these data services can then be used by controllers.
   */
  var dataService = function($http, $q) {
      this.$http = $http;
      this.$q = $q;
  };

  dataService.$inject = ['$http', '$q'];

  dataService.prototype.getTutorial  = function( slug ) {
      var _this = this;

      return _this.$http.get('http://sherman.library.nova.edu/sites/learn/wp-json/tutorials/v2/tutorial/?slug=' + slug )
          .then(function(response){
              if (typeof response.data === 'object') {
                  return response.data;
              } else {
                  return response.data;
              }

          }, function(response) {
              return _this.$q.reject(response.data);
          });
  };


    angular.module('moduleApp').service('dataService', dataService);

})();


(function(){

    'use strict';

    var ModuleController = function( $attrs, dataService, $rootScope, $sce ) {

      var vm        = this;
          vm.slug   = $attrs.name;
          vm.video  = document.querySelector( '.module[data-name="' +  vm.slug + '"] video');

      vm.watch = function( event ) {

        vm.modalToggle = !vm.modalToggle;
        event.preventDefault();

        if ( vm.video.paused ) {
          vm.video.play();
        } else {
          vm.video.pause();
        }

      }

      /**
       * Uses dataService.prototype.getFeatures - be sure to remove if this controller is removed too
       */
      function  getTutorial( slug ){

          dataService.getTutorial( slug ).then(function(data){

              if( data ) {
                vm.tutorial = data[0];
                vm.videoUrl    = $sce.trustAsResourceUrl( "http://www.nova.edu/library/video/" + vm.tutorial.video + ".mp4" );
              }

          });

      }

      getTutorial( vm.slug );

    }

    ModuleController.$inject = [ '$attrs', 'dataService', '$rootScope', '$sce' ];
    angular.module('moduleApp').controller('ModuleController', ModuleController);

})();
(function() {

  angular.module( 'moduleApp' ).filter( 'trusted', ['$sce', function( $sce ) {
    return function( url ) {
      return $sce.trustAsResourceUrl( url );
    }
  }]);

})();

</script>

<style>
  .module {
    position: relative;
  }

  .module:not( .type-module )::before {
    background-color: rgba( 255, 255, 255, 0.3 );
    border: 1px solid #ddd;
    border-radius: 50%;
    color: #aaa;
    content: attr( data-order );
    height: 44px;
    line-height: 44px;
    position: absolute;
    text-align: center;
    top: 33%;
    width: 44px;
  }

  .module .modal-inner {

  }
</style>
