<?php get_header(); ?>
			
	<div id="content">
		
	    <main id="main" role="main">
			
			<header class="clearfix has-background background-base hero">
				<div class="center-grid col-md--eightcol">
					<h1 class="beta">Tag Archive</h1>
					<p class="no-margin">Videos tagged <strong>"<?php single_cat_title(); ?></strong>"</p>
				</div>
			</header>

			<div class="has-cards hero">

			    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
				<div class="center-grid col-md--eightcol" style="margin-bottom: 1em;">

				    <article id="post-<?php the_ID(); ?>" <?php post_class('card--alt media clearfix'); ?> role="article">
						
							<div class="col-md--twocol">
								<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
									<?php echo ( has_post_thumbnail() ? the_post_thumbnail( 'media-small' ) : '<img src=http://placehold.it/640x360>' ); ?>
								</a>
							</div>

							<div class="col-md--tencol col--last">
								<header>
							    	<h2 class="delta"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
						    	</header>

						    <section class="post-content clearfix">						
								   <p class="zeta"><?php echo get_the_excerpt(); ?></p>
						    </section> <!-- end article section -->

						    <footer class="align-right small-text">
						    	<?php the_tags( '' ); ?>
						    </footer>
						    </div>
				
					    </article> <!-- end article -->
				    </div><!--/.center-grid-->
			
			    <?php endwhile; ?>	
			
			        <?php if (function_exists('bones_page_navi')) { // if experimental feature is active ?>
				
				        <?php bones_page_navi(); // use the page navi function ?>

			        <?php } else { // if it is disabled, display regular wp prev & next links ?>
				        <nav class="wp-prev-next">
					        <ul class="clearfix">
						        <li class="prev-link"><?php next_posts_link(_e('&laquo; Older Entries', "bonestheme")) ?></li>
						        <li class="next-link"><?php previous_posts_link(_e('Newer Entries &raquo;', "bonestheme")) ?></li>
					        </ul>
			    	    </nav>
			        <?php } ?>
			
			    <?php else : ?>

				    <article id="post-not-found" class="hentry clearfix">
					    <header class="article-header">
						    <h2 class="hide-accessible"><?php _e("No Search Results!", "bonestheme"); ?></h2>
				    	</header>
				    	<p class="delta">
				    		Oops. Your search for <q><strong><?php echo esc_attr(get_search_query()); ?></strong></q>
				    		didn't work out. 
				    	</p>

			    	</article>
			
			    <?php endif; ?>

			</div><!--/.has-cards-->

		</main> <!-- end #main -->
        
        
	</div> <!-- end #content -->

<?php get_footer(); ?>