<?php get_header(); ?>

<style type="text/css">
	
	.feature-image {
		background-image: url('http://sherman.library.nova.edu/sites/learn/files/2014/04/studious-student-350x193.jpg');
	}

	@media only screen and (min-width: 45em ) {
		.feature-image {
			background-image: url('http://sherman.library.nova.edu/sites/learn/files/2014/04/studious-student-720x405.jpg');
		}
	}

	@media only screen and (min-width: 64em ) {
		.feature-image {
			background-image: url('http://sherman.library.nova.edu/sites/learn/files/2014/04/studious-student-1140x641.jpg');
		}
	}

	@media only screen and (min-width: 77.5em ) {
		.feature-image {
			background-image: url('http://sherman.library.nova.edu/sites/learn/files/2014/04/studious-student.jpg');
		}
	}
	
</style>

<div class="feature feature-image" id="feature">
	<div id="inner-feature" class="clearfix">

			<div class="caption shadow">
			<article class="wrap clearfix">
					<header>
						<h3 class="page-title gamma">Little Videos, Big Impact</h3>
					</header>
					<p class="epsilon">
						LibraryLearn has short video tutorials on a variety of library resources. The videos can show you how to use specific tools like
						NovaCat (our catalog), FindIt, Journal Finder, or even help guide you through the research process. 
					</p>
			</article>
			</div>
	</div><!--.inner-feature-->
</div>

<!-- Menu
======================
--> <nav id="panels" class="panels align-center clearfix" role="navigation">
							
		<a class="panel one-third guides" href="http://sherman.library.nova.edu/sites/learn/module/getting-started-with-research/">
			<span class="panel__link">Getting Started with Research</span>
		</a>
		

		<a class="panel one-third research" href="http://sherman.library.nova.edu/sites/learn/module/advanced-research-tools/">
			<span class="panel__link">Advanced Research Tutorials</span>
		</a>

		<a class="panel one-third citation-management" href="<?php echo esc_url( get_category_link( get_cat_id( 'Citation Management' ) )); ?>">
			<span class="panel__link">Citation Management</span>
		</a>

	</nav>

<!-- Content!
======================
-->	<div id="content">
	
		<div id="inner-content" class="wrap clearfix">
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->



<?php get_footer(); ?>
